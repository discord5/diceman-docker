
FROM alpine:3
ENV DISCORD_TOKEN=

RUN apk update
RUN apk add git
RUN apk add nodejs
RUN addgroup -S diceman --gid 1000 && adduser --uid 1000 -S diceman -G diceman
RUN ln -s /home/diceman/Diceman /server
ADD init.sh /
RUN chown 1000:1000 /init.sh

USER diceman
WORKDIR /home/diceman
RUN git clone https://github.com/TrevorMellon/Diceman.git
WORKDIR /server

ENTRYPOINT [ "/init.sh" ]
