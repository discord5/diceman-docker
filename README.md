# Diceman Discord Bot for Docker

This is [diceman bot](https://github.com/TrevorMellon/Diceman) but wrapped in a nice and comfy docker container.

# Instructions

Container is really simple and doesn't expose anything, so call something like

```bash
docker build -t diceman
docker run -d --restart unless-stopped -e DISCORD_TOKEN=INSERT_YOUR_TOKEN_HERE diceman
```

The only thing that's not obvious is the DISCORD_TOKEN. You can apply for yours at [Discord Dev Portal](https://discordapp.com/developers/applications)
